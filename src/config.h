/**
 @file config.h
 @brief Definition der Klasse Config, welche zum Abspeichern von Einstellungen dienen soll.
 @author Alexey Elimport
 @version 0.1
 @date 16.05.20012
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "singleton.h"

#include <wx/fileconf.h>
#include <wx/panel.h>
#include <wx/hashmap.h>

#include <map>

/**
	@addtogroup SDK
	@class Config config.h "config.h"
	@brief Klasse zur Verwaltung der Konfigurationsdatei. Diese Klasse ist als Singleton Config
    definiert und kann von überall aufgerufen.
*/
class Config: public Singleton<Config>
{
public:
    
    /**
        @brief Konstruktor
     */
	Config();
    
    /**
        @brief Destruktor
     */
	~Config();

	/**
		@brief Laden der Einstellungen aus der Konfigurationsdatei von
		der Gruppe die unter dem Namen des Panels gespeichert ist. Die Elemente auf dem Panel
		bekommen somit die gespeicherten Einstellungen.
		@param panel Das Panel, von dem die Einstellungen hergestellt werden müssen.
		@param c_path Der Pfad zu der Gruppe, wo die Einstellungen gespeichert sind.
	*/
	void ReadSettings(wxWindow* w, wxString c_path);

	/**
		@brief Speichern der Einstellungen der Elemente aus dem Panel in die Konfigurationsdatei.
		@param panel Das Panel, von dem die Einstellungen gespeichert werden müssen.
		@param c_path Der Pfad zu der Gruppe, wo die Einstellungen gespeichert werden.
	*/
	void WriteSettings(wxWindow* w, wxString c_path);

	/**
		@brief Die Konfigurationsdatei speichern.
	*/
	void WriteSettings();

	/**
		@brief Einlesen eines booleschen Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param def Der Wert.
        @param der gelesene Wert falls erfolgreich, sonst def
	*/
	bool ReadBool(wxString path, wxString key, bool def);

	/**
		@brief Einlesen eines double-Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param def Der Wert.
        @return der gelesene Wert falls erfolgreich, sonst def
	*/
	double ReadDouble(wxString path, wxString key, double def);

	/**
		@brief Einlesen eines long-Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param def Der Wert.
        @return der gelesene Wert falls erfolgreich, sonst def
	*/
	long ReadLong(wxString path, wxString key, long def);

	/**
		@brief Einlesen eines wxString-Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param def Der Wert. (Standard: leere Zeichenkette "")
        @return der gelesene Wert falls erfolgreich, sonst def
	*/
	wxString Read(wxString path, wxString key, wxString def="");


	/**
		@brief Speichern eines booleschen Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param value Der Wert.
	*/
	void WriteBool(wxString path, wxString key, bool value);

	/**
		@brief Speichern eines double-Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param value Der Wert.
	*/
	void WriteDouble(wxString path, wxString key, double value);

	/**
		@brief Speichern eines long-Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param value Der Wert.
	*/
	void WriteLong(wxString path, wxString key, long value);

	/**
		@brief Speichern eines wxString-Wertes.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param value Der Wert.
	*/
	void Write(wxString path, wxString key, wxString value);

	/**
		@brief Einen Eintrag aus der Konfigurationsdatei löschen.
		@param path Pfad zur Gruppe, wo der Eintrag gespeichert ist.
		@param key Schlüssel zu dem Wert.
		@param deleteGroupIfEmpty Löschen der Gruppe, wenn diese Leer ist.
	*/
	void DeleteEntry(wxString path, wxString key, bool deleteGroupIfEmpty);

	/**
		@brief Löschen einer ganzen Gruppe aus der Konfigurationsdatei.
		@param name Der Name der Gruppe
	*/
	void DeleteGroup(wxString name);

	/**
		@brief Den ersten Eintrag aus einer Gruppe einlesen.
		@param path Pfad zur Gruppe.
		@param value Der Wert aus dem ersten Eintrag.
		@param index Position in der Gruppe.
        @return TRUE, wenn einen nächsten Wert gibt, sonst FALSE
	*/
	bool GetFirstEntry(wxString path, wxString &value, long &index);


	/**
		@brief Nächsten Eintrag aus der Gruppe einlesen.
		@param path Pfad zur Gruppe.
		@param value Der Wert aus diesem Eintrag.
		@param index Position in der Gruppe.
        @return TRUE, wenn einen nächsten Wert gibt, sonst FALSE
	*/
	bool GetNextEntry(wxString path, wxString &value, long &index);

    /**
        @brief Gibt an, wieviele Subeinträge die Gruppe besitzt.
        @param path Pfad zur Gruppe
        @return Anzahl der Subeinträge
     */
    unsigned int GetNumberOfEntries(wxString path);
	
	wxStringToStringHashMap GetGroupEntries(wxString path);

private:
    
    /**
        @brief zur Synchronisation
     */
	wxMutex m_mutex;
    
    /**
        @brief Hält die Konfigurationsimplementierung der Widgets
     */
	wxFileConfig* m_config;
};

#endif
