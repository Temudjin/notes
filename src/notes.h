#pragma once
#include "notes_frame.h"

#include <wx/wx.h>
#include <wx/process.h>

class Notes: public wxApp
{
    virtual bool OnInit();
    
private:
    NotesFrame* m_frame;
	wxString GetCwd();
};

DECLARE_APP(Notes)