/**
	 @file singleton.h
	 @brief Definiert ein Template mit dem man eine Singleton-Klasse implementieren kann.
	 @author Alexey Elimport
	 @version 0.1
	 @date 16.05.2012
 */

#ifndef SINGLETON_H
#define SINGLETON_H
#include <wx/wx.h>
#include <wx/thread.h>

/**
	 @class Singleton singleton.h "singleton.h"
	 @tparam T Der Datentyp des Singleton.
	 @brief Ein Template für die Definition der Klassen als Singleton.
 */
template<typename T>
class Singleton {
public :
	/** 
        @brief Gibt die Instanz des Objektes zurück (diese wird nur ein mal instanziiert) */
    static T* Get();

	/** 
        @brief Zerstört das Objekt
    */
    static void Destroy();

protected :
    /**
        @brief Konstruktor
     */
    inline Singleton(){}
    
    /**
        @brief Destruktor
     
        Zerstört die Instanz wieder
     */
    inline ~Singleton() {
        Singleton::p_instance = NULL;
    }

private :
    /**
        @brief Kopierkonstruktor
        @param 
     */
    Singleton(Singleton const&){}
    
    /**
        @brief überladener Zuweisungsoperator
        @return Singleton-Instanz
     */
    Singleton& operator=(Singleton const&){return *this;}

private:
    /**
        @brief Instanz
     */
    static T* p_instance;

protected:
    /**
        @brief für Synchronisation
     */
    static wxCriticalSection s_cs;
};    //    end of class Singleton

/**
	Double-Checked locking für die Erstellung der Instanz in einem Multithread-Programm.
	Das Double-Checking ist dafür da, die Resourcen, die für die Synchronisation benötigt
	werden zu minimieren. Somit wird Synchronisation nur dann durchgeführt, wenn die Instanz
	des Singleton das erste mal angelegt wird.
 */
/**
    @brief Gibt Instanz zurück
 
    Sollte noch keine Instanz existieren, so wird eine neue erstellt. ansosnten wird die vorhandene
    zurückgegeben.
    
    @return Instanz
 */
template<typename T>
T* Singleton<T>::Get() {
    if ( Singleton::p_instance == NULL )
    {
		wxCriticalSectionLocker lock(s_cs);
		if(Singleton::p_instance == NULL)
		{
			Singleton::p_instance = new T();
		}
    }
    return Singleton::p_instance;
}

/**
    @brief Zerstört die Instanz
 */
template<typename T>
void Singleton<T>::Destroy() {
    if ( Singleton::p_instance != NULL ) {
        delete Singleton::p_instance;
        Singleton::p_instance = NULL;
    }
}

template<typename T>
T* Singleton<T>::p_instance = NULL;

template<typename T>
wxCriticalSection Singleton<T>::s_cs;


#endif
