///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Nov 27 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __FORMS_H__
#define __FORMS_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/listbox.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/srchctrl.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/listctrl.h>
#include <wx/splitter.h>
#include <wx/frame.h>
#include <wx/valgen.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/dialog.h>
#include <wx/spinctrl.h>
#include <wx/statbox.h>
#include <wx/valtext.h>

///////////////////////////////////////////////////////////////////////////

#define wxID_IMPORT_STUDENTS 1000
#define wxID_EXPORT_STUDENTS 1001
#define wxID_ADD_STUDENT 1002
#define wxID_REMOVE_STUDENT 1003
#define wxID_SEARCH 1004
#define wxID_MENU_SHOW_NAMES 1005
#define wxID_ADD_GROUP 1006
#define wxID_REMOVE_GROUP 1007
#define wxID_TASK_FIELD 1008

///////////////////////////////////////////////////////////////////////////////
/// Class NotesForm
///////////////////////////////////////////////////////////////////////////////
class NotesForm : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* m_menubar1;
		wxMenu* m_menu1;
		wxMenu* m_menu2;
		wxMenu* m_menu3;
		wxMenuItem* m_mnuUserView;
		wxSplitterWindow* m_spnlRoot;
		wxPanel* m_pnlGroups;
		wxListBox* m_listGroups;
		wxButton* m_btnAddGroup;
		wxButton* m_btnRemoveGroup;
		wxPanel* m_pnlStudents;
		wxSearchCtrl* m_txtSearch;
		wxTextCtrl* m_txtTaskString;
		wxStaticText* m_lblTaskNumber;
		wxBoxSizer* m_szrTable;
		wxListCtrl* m_listStudetns;
		wxPanel* m_pnlTasks;
		wxButton* m_btnAddStudent;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnMenu( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLeaveGroups( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnSelectedGroup( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButton( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSearch( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTaskChanging( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTaskEntered( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSorting( wxListEvent& event ) { event.Skip(); }
		virtual void OnStudentDeselected( wxListEvent& event ) { event.Skip(); }
		virtual void OnStudentSelected( wxListEvent& event ) { event.Skip(); }
		
	
	public:
		
		NotesForm( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Noten"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( -1,-1 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~NotesForm();
		
		void m_spnlRootOnIdle( wxIdleEvent& )
		{
			m_spnlRoot->SetSashPosition( 200 );
			m_spnlRoot->Disconnect( wxEVT_IDLE, wxIdleEventHandler( NotesForm::m_spnlRootOnIdle ), NULL, this );
		}
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class StudentForm
///////////////////////////////////////////////////////////////////////////////
class StudentForm : public wxDialog 
{
	private:
	
	protected:
		wxPanel* m_pnlStudentForm;
		wxStaticText* m_staticText4;
		wxTextCtrl* m_txtStudentID;
		wxStaticText* m_staticText2;
		wxTextCtrl* m_txtFirtsName;
		wxStaticText* m_staticText3;
		wxTextCtrl* m_txtLastName;
		wxPanel* m_pnlGroupForm;
		wxStaticText* m_staticText5;
		wxTextCtrl* m_txtGroupName;
		wxStaticText* m_staticText6;
		wxTextCtrl* m_txtGroupAbbrv;
		wxStaticText* m_staticText7;
		wxDatePickerCtrl* m_dateCourseBegin;
		wxButton* m_button5;
		wxButton* m_button6;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnButton( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		StudentForm( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Student"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE ); 
		~StudentForm();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class ConfigForm
///////////////////////////////////////////////////////////////////////////////
class ConfigForm : public wxDialog 
{
	private:
	
	protected:
		wxStaticText* m_staticText8;
		wxSpinCtrl* m_spinCtrl1;
		wxStaticText* m_staticText14;
		wxSpinCtrl* m_spinCtrl2;
		wxStaticText* m_staticText16;
		wxStaticText* m_staticText15;
		wxSpinCtrl* m_spinCtrl3;
		wxStaticText* m_staticText17;
		wxStaticText* m_staticText10;
		wxTextCtrl* m_textCtrl8;
		wxStaticText* m_staticText11;
		wxTextCtrl* m_textCtrl9;
		wxStaticText* m_staticText12;
		wxTextCtrl* m_textCtrl10;
		wxStaticText* m_staticText13;
		wxTextCtrl* m_textCtrl11;
		wxStaticText* m_staticText171;
		wxTextCtrl* m_textCtrl111;
		wxButton* m_button7;
		wxButton* m_button8;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnChangeTaskLabel( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButton( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		ConfigForm( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Configurations"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, const wxString& name = wxT("Notes") ); 
		~ConfigForm();
	
};

#endif //__FORMS_H__
