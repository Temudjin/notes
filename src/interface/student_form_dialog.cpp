#include "student_form_dialog.h"

StudentFormDialog::StudentFormDialog( wxWindow* parent )
:
StudentForm( parent )
{

}

int StudentFormDialog::ShowModal(int mode)
{
    m_pnlStudentForm->Hide();
    m_pnlGroupForm->Hide();
    if(mode == SHOW_STUDENT_FORM)
        m_pnlStudentForm->Show();
    else if(mode == SHOW_GROUP_FROM)
        m_pnlGroupForm->Show();
    
    Layout();
    Fit();
    return wxDialog::ShowModal();
}

void StudentFormDialog::OnButton( wxCommandEvent& event )
{
    switch(event.GetId())
    {
        case wxID_CANCEL:
            EndModal(wxID_CANCEL);
            break;
            
        case wxID_SAVE:
            if(m_pnlStudentForm->IsShown())
            {
                m_values.Add(m_txtStudentID->GetValue());
                m_values.Add(m_txtFirtsName->GetValue());
                m_values.Add(m_txtLastName->GetValue());
            }
            else if(m_pnlGroupForm->IsShown())
            {
                m_values.Add(m_txtGroupName->GetValue());
                m_values.Add(m_txtGroupAbbrv->GetValue());
                m_values.Add(m_dateCourseBegin->GetValue().Format());
            }
            EndModal(wxID_SAVE);
            break;
    }
}
