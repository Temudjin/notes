#include "config_dialog.h"
#include "config.h"

#include <wx/regex.h>
#include <wx/log.h>

ConfigDialog::ConfigDialog( wxWindow* parent )
:
ConfigForm( parent )
{
    Config::Get()->ReadSettings(this, "Notes");
}

void ConfigDialog::OnChangeTaskLabel( wxCommandEvent& event )
{
    wxTextCtrl* txt = (wxTextCtrl *)event.GetEventObject();
    wxRegEx reg;
    reg.Compile("[a-zA-Z]");
    if(!txt->GetValue().IsEmpty())
    {
        if(txt->GetValue().Length() > 1)
        {
            txt->ChangeValue("");
            wxLogWarning("Only one character allowed!");
        }
        else if(!reg.Matches(txt->GetValue()))
        {
            txt->ChangeValue("");
            wxLogWarning("Only alpha characters allowed");
        }
    }
}

void ConfigDialog::OnButton( wxCommandEvent& event )
{
    if(event.GetId() == wxID_SAVE)
        Config::Get()->WriteSettings(this, wxGetCwd());
    EndModal(event.GetId());
}
