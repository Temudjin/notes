#ifndef __config_dialog__
#define __config_dialog__

/**
@file
Subclass of ConfigForm, which is generated by wxFormBuilder.
*/

#include "forms.h"

//// end generated include


/** Implementing ConfigForm */
class ConfigDialog : public ConfigForm
{
	protected:
		// Handlers for ConfigForm events.
		void OnChangeTaskLabel( wxCommandEvent& event );
		void OnButton( wxCommandEvent& event );
	public:
		/** Constructor */
		ConfigDialog( wxWindow* parent );
	//// end generated class members
	
};

#endif // __config_dialog__
