///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Nov 27 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "forms.h"

///////////////////////////////////////////////////////////////////////////

NotesForm::NotesForm( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWFRAME ) );
	
	m_menubar1 = new wxMenuBar( 0 );
	m_menu1 = new wxMenu();
	wxMenuItem* m_menuItem5;
	m_menuItem5 = new wxMenuItem( m_menu1, wxID_IMPORT_STUDENTS, wxString( wxT("Import Students") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem5 );
	
	wxMenuItem* m_menuItem6;
	m_menuItem6 = new wxMenuItem( m_menu1, wxID_EXPORT_STUDENTS, wxString( wxT("Export Students") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem6 );
	
	m_menu1->AppendSeparator();
	
	wxMenuItem* m_menuItem7;
	m_menuItem7 = new wxMenuItem( m_menu1, wxID_PREFERENCES, wxString( wxT("Configuration") ) + wxT('\t') + wxT("Ctrl+,"), wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem7 );
	
	wxMenuItem* m_menuItem1;
	m_menuItem1 = new wxMenuItem( m_menu1, wxID_EXIT, wxString( wxT("Quit") ) + wxT('\t') + wxT("Ctrl+Q"), wxEmptyString, wxITEM_NORMAL );
	m_menu1->Append( m_menuItem1 );
	
	m_menubar1->Append( m_menu1, wxT("Notes") ); 
	
	m_menu2 = new wxMenu();
	wxMenuItem* m_menuItem3;
	m_menuItem3 = new wxMenuItem( m_menu2, wxID_ADD_STUDENT, wxString( wxT("Add student") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem3 );
	
	wxMenuItem* m_menuItem4;
	m_menuItem4 = new wxMenuItem( m_menu2, wxID_REMOVE_STUDENT, wxString( wxT("Remove student") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem4 );
	
	m_menu2->AppendSeparator();
	
	wxMenuItem* m_menuItem2;
	m_menuItem2 = new wxMenuItem( m_menu2, wxID_SEARCH, wxString( wxT("Search\tCtrl+F") ) , wxEmptyString, wxITEM_NORMAL );
	m_menu2->Append( m_menuItem2 );
	
	m_menubar1->Append( m_menu2, wxT("Student") ); 
	
	m_menu3 = new wxMenu();
	m_mnuUserView = new wxMenuItem( m_menu3, wxID_MENU_SHOW_NAMES, wxString( wxT("Show names") ) , wxEmptyString, wxITEM_CHECK );
	m_menu3->Append( m_mnuUserView );
	
	m_menubar1->Append( m_menu3, wxT("View") ); 
	
	this->SetMenuBar( m_menubar1 );
	
	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxVERTICAL );
	
	m_spnlRoot = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3DSASH|wxSP_NOBORDER );
	m_spnlRoot->SetSashGravity( 0.07 );
	m_spnlRoot->Connect( wxEVT_IDLE, wxIdleEventHandler( NotesForm::m_spnlRootOnIdle ), NULL, this );
	
	m_pnlGroups = new wxPanel( m_spnlRoot, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxVERTICAL );
	
	m_listGroups = new wxListBox( m_pnlGroups, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, wxLB_SORT ); 
	bSizer6->Add( m_listGroups, 1, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );
	
	m_btnAddGroup = new wxButton( m_pnlGroups, wxID_ADD_GROUP, wxT("Add group"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer7->Add( m_btnAddGroup, 0, wxEXPAND|wxALL, 5 );
	
	m_btnRemoveGroup = new wxButton( m_pnlGroups, wxID_REMOVE_GROUP, wxT("Remove group"), wxDefaultPosition, wxDefaultSize, 0 );
	m_btnRemoveGroup->Hide();
	
	bSizer7->Add( m_btnRemoveGroup, 0, wxEXPAND|wxALL, 5 );
	
	
	bSizer6->Add( bSizer7, 0, wxEXPAND, 5 );
	
	
	m_pnlGroups->SetSizer( bSizer6 );
	m_pnlGroups->Layout();
	bSizer6->Fit( m_pnlGroups );
	m_pnlStudents = new wxPanel( m_spnlRoot, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer61;
	bSizer61 = new wxBoxSizer( wxHORIZONTAL );
	
	m_txtSearch = new wxSearchCtrl( m_pnlStudents, wxID_SEARCH, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), wxTE_PROCESS_ENTER );
	#ifndef __WXMAC__
	m_txtSearch->ShowSearchButton( true );
	#endif
	m_txtSearch->ShowCancelButton( false );
	bSizer61->Add( m_txtSearch, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxHORIZONTAL );
	
	m_txtTaskString = new wxTextCtrl( m_pnlStudents, wxID_TASK_FIELD, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	bSizer18->Add( m_txtTaskString, 1, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_lblTaskNumber = new wxStaticText( m_pnlStudents, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTaskNumber->Wrap( -1 );
	bSizer18->Add( m_lblTaskNumber, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer61->Add( bSizer18, 1, wxEXPAND, 5 );
	
	
	bSizer5->Add( bSizer61, 0, wxEXPAND, 5 );
	
	m_szrTable = new wxBoxSizer( wxVERTICAL );
	
	m_listStudetns = new wxListCtrl( m_pnlStudents, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_HRULES|wxLC_REPORT|wxLC_SINGLE_SEL|wxLC_VRULES );
	m_szrTable->Add( m_listStudetns, 1, wxALL|wxEXPAND, 5 );
	
	m_pnlTasks = new wxPanel( m_pnlStudents, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_szrTable->Add( m_pnlTasks, 0, wxEXPAND|wxRIGHT, 5 );
	
	
	bSizer5->Add( m_szrTable, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );
	
	m_btnAddStudent = new wxButton( m_pnlStudents, wxID_ADD_STUDENT, wxT("Add student"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_btnAddStudent, 0, wxALL, 5 );
	
	
	bSizer5->Add( bSizer9, 0, wxEXPAND, 5 );
	
	
	m_pnlStudents->SetSizer( bSizer5 );
	m_pnlStudents->Layout();
	bSizer5->Fit( m_pnlStudents );
	m_spnlRoot->SplitVertically( m_pnlGroups, m_pnlStudents, 200 );
	bSizer17->Add( m_spnlRoot, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer17 );
	this->Layout();
	bSizer17->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( m_menuItem5->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Connect( m_menuItem6->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Connect( m_menuItem7->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Connect( m_menuItem1->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Connect( m_menuItem2->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Connect( m_mnuUserView->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	m_listGroups->Connect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( NotesForm::OnLeaveGroups ), NULL, this );
	m_listGroups->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( NotesForm::OnSelectedGroup ), NULL, this );
	m_btnAddGroup->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NotesForm::OnButton ), NULL, this );
	m_btnRemoveGroup->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NotesForm::OnButton ), NULL, this );
	m_txtSearch->Connect( wxEVT_COMMAND_SEARCHCTRL_SEARCH_BTN, wxCommandEventHandler( NotesForm::OnSearch ), NULL, this );
	m_txtSearch->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NotesForm::OnSearch ), NULL, this );
	m_txtSearch->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( NotesForm::OnSearch ), NULL, this );
	m_txtTaskString->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NotesForm::OnTaskChanging ), NULL, this );
	m_txtTaskString->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( NotesForm::OnTaskEntered ), NULL, this );
	m_listStudetns->Connect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( NotesForm::OnSorting ), NULL, this );
	m_listStudetns->Connect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( NotesForm::OnStudentDeselected ), NULL, this );
	m_listStudetns->Connect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( NotesForm::OnStudentSelected ), NULL, this );
	m_btnAddStudent->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NotesForm::OnButton ), NULL, this );
}

NotesForm::~NotesForm()
{
	// Disconnect Events
	this->Disconnect( wxID_IMPORT_STUDENTS, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Disconnect( wxID_EXPORT_STUDENTS, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Disconnect( wxID_PREFERENCES, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Disconnect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Disconnect( wxID_SEARCH, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	this->Disconnect( wxID_MENU_SHOW_NAMES, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( NotesForm::OnMenu ) );
	m_listGroups->Disconnect( wxEVT_LEAVE_WINDOW, wxMouseEventHandler( NotesForm::OnLeaveGroups ), NULL, this );
	m_listGroups->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( NotesForm::OnSelectedGroup ), NULL, this );
	m_btnAddGroup->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NotesForm::OnButton ), NULL, this );
	m_btnRemoveGroup->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NotesForm::OnButton ), NULL, this );
	m_txtSearch->Disconnect( wxEVT_COMMAND_SEARCHCTRL_SEARCH_BTN, wxCommandEventHandler( NotesForm::OnSearch ), NULL, this );
	m_txtSearch->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NotesForm::OnSearch ), NULL, this );
	m_txtSearch->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( NotesForm::OnSearch ), NULL, this );
	m_txtTaskString->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( NotesForm::OnTaskChanging ), NULL, this );
	m_txtTaskString->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( NotesForm::OnTaskEntered ), NULL, this );
	m_listStudetns->Disconnect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( NotesForm::OnSorting ), NULL, this );
	m_listStudetns->Disconnect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( NotesForm::OnStudentDeselected ), NULL, this );
	m_listStudetns->Disconnect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( NotesForm::OnStudentSelected ), NULL, this );
	m_btnAddStudent->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( NotesForm::OnButton ), NULL, this );
	
}

StudentForm::StudentForm( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* m_szrForms;
	m_szrForms = new wxBoxSizer( wxVERTICAL );
	
	m_pnlStudentForm = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText4 = new wxStaticText( m_pnlStudentForm, wxID_ANY, wxT("Student ID:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	fgSizer2->Add( m_staticText4, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_txtStudentID = new wxTextCtrl( m_pnlStudentForm, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_txtStudentID, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText2 = new wxStaticText( m_pnlStudentForm, wxID_ANY, wxT("First name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	fgSizer2->Add( m_staticText2, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_txtFirtsName = new wxTextCtrl( m_pnlStudentForm, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 180,-1 ), 0 );
	fgSizer2->Add( m_txtFirtsName, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText3 = new wxStaticText( m_pnlStudentForm, wxID_ANY, wxT("Last name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	fgSizer2->Add( m_staticText3, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_txtLastName = new wxTextCtrl( m_pnlStudentForm, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_txtLastName, 0, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizer10->Add( fgSizer2, 1, wxEXPAND, 5 );
	
	
	m_pnlStudentForm->SetSizer( bSizer10 );
	m_pnlStudentForm->Layout();
	bSizer10->Fit( m_pnlStudentForm );
	m_szrForms->Add( m_pnlStudentForm, 1, wxEXPAND, 5 );
	
	m_pnlGroupForm = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxVERTICAL );
	
	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer3->AddGrowableCol( 1 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText5 = new wxStaticText( m_pnlGroupForm, wxID_ANY, wxT("Group name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	fgSizer3->Add( m_staticText5, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_txtGroupName = new wxTextCtrl( m_pnlGroupForm, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_txtGroupName, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText6 = new wxStaticText( m_pnlGroupForm, wxID_ANY, wxT("Abbrv.:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	fgSizer3->Add( m_staticText6, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_txtGroupAbbrv = new wxTextCtrl( m_pnlGroupForm, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer3->Add( m_txtGroupAbbrv, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText7 = new wxStaticText( m_pnlGroupForm, wxID_ANY, wxT("Begin:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	fgSizer3->Add( m_staticText7, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_dateCourseBegin = new wxDatePickerCtrl( m_pnlGroupForm, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, wxDP_DEFAULT );
	fgSizer3->Add( m_dateCourseBegin, 0, wxALL, 5 );
	
	
	bSizer12->Add( fgSizer3, 1, wxEXPAND, 5 );
	
	
	m_pnlGroupForm->SetSizer( bSizer12 );
	m_pnlGroupForm->Layout();
	bSizer12->Fit( m_pnlGroupForm );
	m_szrForms->Add( m_pnlGroupForm, 0, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxHORIZONTAL );
	
	m_button5 = new wxButton( this, wxID_SAVE, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_button5, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_button6 = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer9->Add( m_button6, 0, wxALL, 5 );
	
	
	m_szrForms->Add( bSizer9, 0, wxALIGN_RIGHT, 5 );
	
	
	this->SetSizer( m_szrForms );
	this->Layout();
	m_szrForms->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_button5->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StudentForm::OnButton ), NULL, this );
	m_button6->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StudentForm::OnButton ), NULL, this );
}

StudentForm::~StudentForm()
{
	// Disconnect Events
	m_button5->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StudentForm::OnButton ), NULL, this );
	m_button6->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StudentForm::OnButton ), NULL, this );
	
}

ConfigForm::ConfigForm( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxDialog( parent, id, title, pos, size, style, name )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer3;
	sbSizer3 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("General") ), wxVERTICAL );
	
	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 0, 2, 0, 0 );
	
	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Tasks count:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	gSizer1->Add( m_staticText8, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl1 = new wxSpinCtrl( this, wxID_ANY, wxT("25"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 50, 10, wxT("TaskCount") );
	gSizer1->Add( m_spinCtrl1, 0, wxALL, 5 );
	
	
	sbSizer3->Add( gSizer1, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );
	
	
	bSizer12->Add( sbSizer3, 0, wxEXPAND|wxALL, 5 );
	
	wxStaticBoxSizer* sbSizer2;
	sbSizer2 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Admition") ), wxVERTICAL );
	
	wxFlexGridSizer* fgSizer5;
	fgSizer5 = new wxFlexGridSizer( 0, 3, 0, 0 );
	fgSizer5->SetFlexibleDirection( wxBOTH );
	fgSizer5->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText14 = new wxStaticText( this, wxID_ANY, wxT("Right tasks:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	fgSizer5->Add( m_staticText14, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl2 = new wxSpinCtrl( this, wxID_ANY, wxT("40"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 100, 1, wxT("RightTasks") );
	fgSizer5->Add( m_spinCtrl2, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText16 = new wxStaticText( this, wxID_ANY, wxT("%"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	fgSizer5->Add( m_staticText16, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Satisfied tasks:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	fgSizer5->Add( m_staticText15, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrl3 = new wxSpinCtrl( this, wxID_ANY, wxT("60"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 100, 0, wxT("SatTasks") );
	fgSizer5->Add( m_spinCtrl3, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticText17 = new wxStaticText( this, wxID_ANY, wxT("%"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	fgSizer5->Add( m_staticText17, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	
	sbSizer2->Add( fgSizer5, 1, wxEXPAND, 5 );
	
	
	bSizer12->Add( sbSizer2, 0, wxEXPAND|wxALL, 5 );
	
	wxStaticBoxSizer* sbSizer1;
	sbSizer1 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Task labels") ), wxVERTICAL );
	
	wxFlexGridSizer* fgSizer4;
	fgSizer4 = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer4->AddGrowableCol( 0 );
	fgSizer4->SetFlexibleDirection( wxBOTH );
	fgSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Right char:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	fgSizer4->Add( m_staticText10, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl8 = new wxTextCtrl( this, wxID_ANY, wxT("r"), wxDefaultPosition, wxSize( 40,-1 ), 0, wxDefaultValidator, wxT("RightChar") );
	fgSizer4->Add( m_textCtrl8, 0, wxALL, 5 );
	
	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("Wrong char:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	fgSizer4->Add( m_staticText11, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl9 = new wxTextCtrl( this, wxID_ANY, wxT("f"), wxDefaultPosition, wxSize( 40,-1 ), 0, wxDefaultValidator, wxT("WrongChar") );
	fgSizer4->Add( m_textCtrl9, 0, wxALL, 5 );
	
	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Satisfied char:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	fgSizer4->Add( m_staticText12, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl10 = new wxTextCtrl( this, wxID_ANY, wxT("m"), wxDefaultPosition, wxSize( 40,-1 ), 0, wxDefaultValidator, wxT("SatChar") );
	fgSizer4->Add( m_textCtrl10, 0, wxALL, 5 );
	
	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Deficient char:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	fgSizer4->Add( m_staticText13, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl11 = new wxTextCtrl( this, wxID_ANY, wxT("u"), wxDefaultPosition, wxSize( 40,-1 ), 0, wxDefaultValidator, wxT("DefChar") );
	fgSizer4->Add( m_textCtrl11, 0, wxALL, 5 );
	
	m_staticText171 = new wxStaticText( this, wxID_ANY, wxT("Additional char:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText171->Wrap( -1 );
	fgSizer4->Add( m_staticText171, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_textCtrl111 = new wxTextCtrl( this, wxID_ANY, wxT("z"), wxDefaultPosition, wxSize( 40,-1 ), 0, wxDefaultValidator, wxT("AdditionalChar") );
	fgSizer4->Add( m_textCtrl111, 0, wxALL, 5 );
	
	
	sbSizer1->Add( fgSizer4, 1, wxEXPAND, 5 );
	
	
	bSizer12->Add( sbSizer1, 1, wxEXPAND|wxALL, 5 );
	
	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxHORIZONTAL );
	
	m_button7 = new wxButton( this, wxID_SAVE, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button7->SetDefault(); 
	bSizer14->Add( m_button7, 0, wxALL, 5 );
	
	m_button8 = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer14->Add( m_button8, 0, wxALL, 5 );
	
	
	bSizer12->Add( bSizer14, 0, wxALIGN_RIGHT|wxBOTTOM, 5 );
	
	
	this->SetSizer( bSizer12 );
	this->Layout();
	bSizer12->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_textCtrl8->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_textCtrl9->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_textCtrl10->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_textCtrl11->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_button7->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConfigForm::OnButton ), NULL, this );
	m_button8->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConfigForm::OnButton ), NULL, this );
}

ConfigForm::~ConfigForm()
{
	// Disconnect Events
	m_textCtrl8->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_textCtrl9->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_textCtrl10->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_textCtrl11->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( ConfigForm::OnChangeTaskLabel ), NULL, this );
	m_button7->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConfigForm::OnButton ), NULL, this );
	m_button8->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConfigForm::OnButton ), NULL, this );
	
}
