#include "notes_frame.h"
#include "student_form_dialog.h"
#include "config_dialog.h"
#include "config.h"

#include <wx/wxsqlite3.h>
#include <wx/wx.h>
#include <wx/dir.h>
#include <wx/textfile.h>
#include <wx/propgrid/propgrid.h>
#include <wx/grid.h>

#include <vector>

NotesFrame::NotesFrame( wxWindow* parent )
:
NotesForm( parent )
{
    m_showNames = Config::Get()->ReadBool("Notes", "ShowNames", true);
	m_mnuUserView->Check(m_showNames);
    SetSize(600, 300);
    LoadSettings();
    PrepareData();
    LoadData();
    Layout();
    Bind(wxEVT_COMMAND_TEXT_UPDATED, &NotesFrame::OnSearch, this, wxID_ANY);
    m_txtSearch->Bind(wxEVT_COMMAND_TEXT_ENTER, &NotesFrame::OnSearch, this, wxID_SEARCH);
	m_txtSearch->Bind(wxEVT_COMMAND_TEXT_UPDATED, &NotesFrame::OnSearch, this, wxID_SEARCH);
    m_txtTaskString->Bind(wxEVT_COMMAND_TEXT_ENTER, &NotesFrame::OnTaskEntered, this, wxID_ANY);
    m_listStudetns->Bind(wxEVT_COMMAND_LIST_COL_CLICK, &NotesFrame::OnSort, this, wxID_ANY);

    Maximize();
}

void NotesFrame::OnSelectedGroup( wxCommandEvent& event )
{
    m_btnRemoveGroup->Show();
    m_pnlGroups->Layout();
    m_listStudetns->ClearAll();
    
    try{
        wxString sql = wxString::Format("SELECT * FROM students,groups WHERE \
                                        students.group_id = groups.group_id AND \
                                        groups.name = \"%s\"", m_listGroups->GetString(event.GetSelection()));
        wxSQLite3Table students = m_db->GetTable(sql);
        
        PrepareData();

        for(size_t i=0,j=students.GetRowCount();i<students.GetRowCount();++i,--j)
        {
            students.SetRow(i);
            int idx = -1;
            idx = m_listStudetns->InsertItem(0, wxString::Format("%d",(int)j));
            m_listStudetns->SetItem(idx, 1, students.GetString(1));
            m_listStudetns->SetItem(idx, 2, students.GetString(2));
            m_listStudetns->SetItem(idx, 3, students.GetString(0));
            bool admit = CalculateStatistic(idx, students.GetString(4));
            if(admit)
                m_listStudetns->SetItemBackgroundColour(0, wxColour(190, 255, 190));
            else
                m_listStudetns->SetItemBackgroundColour(0, wxColour(255, 190, 190));
        }
        
    }
    catch(wxSQLite3Exception e)
    {
        wxLogMessage(e.GetMessage());
    }
    
}

void NotesFrame::OnButton( wxCommandEvent& event )
{
    m_btnRemoveGroup->Hide();
    m_pnlGroups->Layout();
    switch (event.GetId()) {
        case wxID_ADD_GROUP:AddGroup();
            break;
        case wxID_REMOVE_GROUP:RemoveGroup();
            break;
        case wxID_ADD_STUDENT:AddStudent();
            break;  
    }
}

void NotesFrame::OnSearch( wxCommandEvent& event )
{
    if(!m_txtTaskString->IsEmpty())
        m_txtTaskString->Clear();
    if(event.GetId() == wxID_SEARCH)
		ShowFiltered(m_txtSearch->GetValue());
}

void NotesFrame::OnSort(wxListEvent& event)
{
    int idx = event.GetColumn();
    wxListItem column;
    m_listStudetns->GetColumn(idx, column);
    const char *col = column.GetText().data().AsChar();
}

void NotesFrame::OnTaskEntered(wxCommandEvent& event)
{
    if(event.GetId() == wxID_TASK_FIELD)
    {
        SaveTaskString();
        if(!m_txtSearch->IsEmpty())
            ShowFiltered(m_txtSearch->GetValue());
        else
            LoadData();
        const char * search = m_txtSearch->GetValue().data().AsChar();
        Layout();
        m_txtTaskString->SetForegroundColour(*wxBLACK);
    }
}

void NotesFrame::OnTaskChanging(wxCommandEvent &event)
{
    wxString value = m_txtTaskString->GetValue();

    if(!CheckString(value))
    {
        wxLogWarning("String is not correct. Check the syntax");
        value = value.SubString(0, value.Length()-2);
        m_txtTaskString->ChangeValue(value);
    }
    else
        m_lblTaskNumber->SetLabel(wxString::Format("%d", (int)value.Length()));
    m_txtTaskString->SetForegroundColour(wxColour(0, 220, 0));
    m_pnlStudents->Layout();
}


void NotesFrame::OnMenu( wxCommandEvent& event )
{
    switch(event.GetId())
    {
        case wxID_EXIT:wxExit();break;
        case wxID_SEARCH:
            m_txtSearch->SetFocus();
            m_txtSearch->SelectAll();
            break;
        case wxID_PREFERENCES:
        {
            ConfigDialog cfg(this);
            if(cfg.ShowModal() == wxID_SAVE)
            {
                LoadSettings();
                LoadData();
            }
            break;
        }
        case wxID_IMPORT_STUDENTS:
            ImportCSV();
            break;
        case wxID_EXPORT_STUDENTS:
            ExportCSV();
            break;
        case wxID_MENU_SHOW_NAMES:
            m_showNames = event.IsChecked();
            Config::Get()->WriteBool("Notes", "ShowNames", m_showNames);
            LoadData();
            break;
            
    }
}

void NotesFrame::OnStudentSelected(wxListEvent &event)
{
    m_itemSelected = event.GetItem();
    wxListItem id = event.GetItem();
    id.SetColumn(3);
    m_listStudetns->GetItem(id);
    LoadTaskString(id.GetText());
    m_pnlStudents->Layout();
    Layout();
}

void NotesFrame::OnStudentDeselected(wxListEvent &event)
{
    m_itemSelected = event.GetItem();
}


//----------------------------------------------------------------------
// Methods
//----------------------------------------------------------------------

void NotesFrame::LoadSettings()
{
    m_tCount = Config::Get()->ReadLong("/Notes", "TaskCount", 28);
    m_rTasks = Config::Get()->ReadLong("/Notes", "RightTasks", 40);
    m_sTasks = Config::Get()->ReadLong("/Notes", "SatTasks", 60);
    _f = Config::Get()->ReadLong("/Notes", "WrongChar", 'f');
    _r = Config::Get()->ReadLong("/Notes", "RightChar", 'r');
    _u = Config::Get()->ReadLong("/Notes", "DefChar", 'u');
    _m = Config::Get()->ReadLong("/Notes", "SatChar", 'm');
    _z = Config::Get()->ReadLong("/Notes", "AdditionalChar", 'z');
}

void NotesFrame::ImportCSV()
{
    if(m_listGroups->GetSelection() == wxNOT_FOUND)
    {
        wxLogWarning("Select the group to insert the students into");
        return;
    }
    wxString fname,lname,id;
    wxString group = m_listGroups->GetString(m_listGroups->GetSelection());
    wxFileDialog dlg(this, _("Select the CSV-File"), "", "",
                     "CSV-File (*.csv)|*.csv", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if(dlg.ShowModal() == wxID_CANCEL)
        return;
    wxString path = dlg.GetPath();
    wxTextFile csv(path);
    if(!csv.Open())
    {
        wxLogError("The file could not be opened");
        return;
    }
    wxString line = csv.GetFirstLine();
    while(!csv.Eof())
    {
        wxStringTokenizer tokenz(line, ";");
        line = csv.GetNextLine();
        std::vector<wxString> columns;

        while(tokenz.HasMoreTokens())
        {
            columns.push_back(tokenz.GetNextToken());
        }
        if(columns.size() < 3)
        {
            wxLogWarning("Empty cell by a user");
            for(size_t i = columns.size();i<4;++i)
                columns.push_back(wxEmptyString);
        }
        m_db->Begin();
        InsertStudent(columns[2], columns[1], columns[0], group);
        m_db->Commit();
    }
    LoadData();
}

void NotesFrame::ExportCSV()
{
    wxFileDialog dlg(this, _("Select the CSV-File"), "", "",
                     "CSV-File (*.csv)|*.csv", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
    if(dlg.ShowModal() == wxID_CANCEL)
        return;
    
    wxString path = dlg.GetPath();
    wxTextFile csv(path);
    if(wxFile::Exists(path))
        wxRemoveFile(path);

    csv.Create();
    if(!csv.Open())
    {
        wxLogError("The file could not be opened");
        return;
    }
    size_t rcount = m_listStudetns->GetItemCount();
    
    for(size_t r=0;r<rcount;++r)
    {
        wxListItem row;
        row.SetId(r);
        
        size_t ccount = m_listStudetns->GetColumnCount();
        wxString line;
        wxString columns;
        for(size_t c=0;c<ccount;++c)
        {
            row.m_col = c;
            m_listStudetns->GetItem(row);
            
            // Hinzufügen der Spalten
            if(r == 0)
            {
                wxListItem column;
                m_listStudetns->GetColumn(c, column);
                columns.Append(column.GetText()+";");
            }
            line.Append(row.GetText()+";");
        }
        if(r == 0)
            csv.AddLine(columns);
        csv.AddLine(line);
    }
    csv.Write();
    csv.Close();
}

void NotesFrame::PrepareData()
{
    m_listStudetns->InsertColumn(0, "Nr");
    m_listStudetns->InsertColumn(1, "First name");
    m_listStudetns->InsertColumn(2, "Last name");
    m_listStudetns->InsertColumn(3, "Student ID");
    m_listStudetns->InsertColumn(4, "Finished", wxLIST_FORMAT_LEFT, 60);
    m_listStudetns->InsertColumn(5, "Finished (%)", wxLIST_FORMAT_LEFT, 60);
    
    m_listStudetns->InsertColumn(6, "Correct", wxLIST_FORMAT_LEFT, 60);
    m_listStudetns->InsertColumn(7, "Satisfied", wxLIST_FORMAT_LEFT, 60);
    m_listStudetns->InsertColumn(8, "Sum", wxLIST_FORMAT_LEFT, 60);
    
    m_listStudetns->InsertColumn(9, "Wrong", wxLIST_FORMAT_LEFT, 60);
    m_listStudetns->InsertColumn(10,"Deficient", wxLIST_FORMAT_LEFT, 60);
    m_listStudetns->InsertColumn(11,"Sum", wxLIST_FORMAT_LEFT, 60);
    
    m_listStudetns->InsertColumn(12, "Admitted");
    
    m_db = new wxSQLite3Database();
	wxString cwd = wxGetCwd();
	wxString path = wxGetCwd() + wxFileName::GetPathSeparator() + "notes.db";
	m_db->Open(path);
}

void NotesFrame::ShowFiltered(wxString query)
{
    if(query.Contains("?"))
    {
        int pos = query.Find('?');
        query.SetChar(pos, '_');
        
    }
    if(query.Contains("*"))
    {
        int pos = query.Find('*');
        query.SetChar(pos, '%');
    }
    else
    {
        query.Append("%");
    }
    if (query.IsEmpty()) {
        LoadData();
        return;
    }
    m_listStudetns->ClearAll();
    
    PrepareData();
    
    try{
        wxString sql = wxString::Format("SELECT * FROM students WHERE \
                                        students.first_name LIKE '%s' OR \
                                        students.last_name LIKE '%s' OR \
                                        students.student_id LIKE '%s'", query, query, query);
        wxSQLite3Table students = m_db->GetTable(sql);
        
        for(size_t i=0,j=students.GetRowCount();i<students.GetRowCount();++i,--j)
        {
            int idx = -1;
            students.SetRow(i);
            idx = m_listStudetns->InsertItem(0, wxString::Format("%d",(int)j));
			if(m_showNames)
			{
				m_listStudetns->SetItem(idx, 1, students.GetString(1));
				m_listStudetns->SetItem(idx, 2, students.GetString(2));
			}
			else
			{
				m_listStudetns->SetItem(idx, 1, "");
				m_listStudetns->SetItem(idx, 2, "");
			}
            m_listStudetns->SetItem(idx, 3, students.GetString(0));
            bool admit = CalculateStatistic(idx, students.GetString(4));
            if((i % 2) == 0)
            {
                if(admit)
                    m_listStudetns->SetItemBackgroundColour(0, wxColour(180, 255, 180));
                else
                    m_listStudetns->SetItemBackgroundColour(0, wxColour(255, 180, 180));
            }
            else
            {
                if(admit)
                    m_listStudetns->SetItemBackgroundColour(0, wxColour(220, 255, 220));
                else
                    m_listStudetns->SetItemBackgroundColour(0, wxColour(255, 220, 220));
            }
        }
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
}

void NotesFrame::LoadData(wxString id)
{
    m_listGroups->Clear();
    m_listStudetns->ClearAll();
    try{
        wxSQLite3Table groups,students;
        groups = m_db->GetTable("SELECT * FROM groups");
        for(size_t i=0;i<groups.GetRowCount();++i)
        {
            groups.SetRow(i);
            m_listGroups->Append(groups.GetString("name"));
        }
        wxString query;
        if(id.IsEmpty())
            query = "SELECT * FROM students";
        else
            query = "SELECT * FROM students WHERE student_id = " + id;
        students = m_db->GetTable(query);

        PrepareData();
        
        for(size_t i=0,j=students.GetRowCount();i<students.GetRowCount();++i,--j)
        {
            int idx = -1;
            students.SetRow(i);
            idx = m_listStudetns->InsertItem(0, wxString::Format("%d",(int)j));
            if(m_showNames)
            {
                m_listStudetns->SetItem(idx, 1, students.GetString(1));
                m_listStudetns->SetItem(idx, 2, students.GetString(2));
            }
            else
            {
                m_listStudetns->SetItem(idx, 1, "");
                m_listStudetns->SetItem(idx, 2, "");
            }
            m_listStudetns->SetItem(idx, 3, students.GetString(0));
            
            bool admit = CalculateStatistic(idx, students.GetString(4));
            if(!id.IsEmpty())
                m_txtTaskString->SetValue(students.GetString(4));
            if(admit)
                m_listStudetns->SetItemBackgroundColour(0, wxColour(180, 255, 180));
            else
                m_listStudetns->SetItemBackgroundColour(0, wxColour(255, 190, 190));
        }
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
}

void NotesFrame::AddGroup()
{
    wxArrayString values;
    StudentFormDialog dlg(this);
    if(dlg.ShowModal(SHOW_GROUP_FROM) != wxID_SAVE)
        return;
    
    values = dlg.GetFormValues();
    wxString name = values.Item(0);
    wxString abbrv = values.Item(1);
    wxDateTime time;
    time.ParseDate(values.Item(2));
    
    try{
        m_db->Begin();
        wxSQLite3Statement stmt = m_db->PrepareStatement("INSERT INTO groups VALUES(null, ?, ?, ?);");
        stmt.Bind(1, name);
        stmt.Bind(2, abbrv);
        stmt.Bind(3, time.Format());
        stmt.ExecuteUpdate();
        stmt.Reset();
        m_db->Commit();
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
    LoadData();
}

void NotesFrame::RemoveGroup()
{
    size_t selection = m_listGroups->GetSelection();
    wxString name = m_listGroups->GetString(selection);
    
    try{
        m_db->Begin();
        m_db->ExecuteUpdate(wxString::Format("DELETE FROM groups WHERE groups.name = '%s'", name));
        m_db->Commit();
        LoadData();
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
}

void NotesFrame::AddStudent()
{
    wxArrayString values;
    StudentFormDialog dlg(this);
    dlg.Fit();
    if(dlg.ShowModal(SHOW_STUDENT_FORM) == wxID_SAVE)
    {
        values = dlg.GetFormValues();
        values.Add(m_listGroups->GetString(m_listGroups->GetSelection()));
    }
    else
        return;
    
    wxString path = wxGetCwd() + wxFileName::GetPathSeparator() + "notes.db";
    m_db->Begin();
    InsertStudent(values[0], values[1], values[2], values[3]);
    m_db->Commit();
    LoadData();
}

void NotesFrame::InsertStudent(wxString id, wxString fname,
                               wxString lname,
                               wxString group)
{
    try
    {
        wxSQLite3Statement stmt = m_db->PrepareStatement("INSERT INTO students(student_id,first_name,last_name,group_id) VALUES\
                                                         (?, ?, ?, (SELECT group_id FROM groups\
                                                         WHERE groups.name=?));");
        stmt.Bind(1, id);
        stmt.Bind(2, fname);
        stmt.Bind(3, lname);
        stmt.Bind(4, group);
        stmt.ExecuteUpdate();
        stmt.Reset();
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
}

void NotesFrame::RemoveStudent()
{
     
}

void NotesFrame::SaveTaskString()
{
    int idx = m_itemSelected;
    if(idx < 0)
    {
        wxLogWarning("Select the student first");
        return;
    }
    wxListItem  id = m_itemSelected;
    id.SetColumn(3);
    m_listStudetns->GetItem(id);
    wxString student_id = id.GetText();
    wxString value = m_txtTaskString->GetValue();
    if(value.IsEmpty() || !CheckString(value))
    {
        wxLogWarning("No tasks were entered!");
        return;
    }
    
    wxRegEx reg;
    reg.Compile("\t");
    reg.Replace(&value, "");
    
    try{
        m_db->Begin();
        wxSQLite3Statement stmt = m_db->PrepareStatement("UPDATE students SET task_string=? WHERE student_id=?;");
        stmt.Bind(1, value);
        stmt.Bind(2, student_id);
        stmt.ExecuteUpdate();
        stmt.Reset();
        m_db->Commit();
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
}

void NotesFrame::LoadTaskString(wxString id)
{
    wxString student_id;
    if(id.IsEmpty())
    {
        wxListItem  id = m_itemSelected;
        id.SetColumn(2);
        m_listStudetns->GetItem(id);
        student_id = id.GetText();
    }
    else
    {
        student_id = id;
    }
    
    wxString value;
    try{
        wxSQLite3ResultSet result = m_db->ExecuteQuery("SELECT task_string FROM students WHERE student_id=" + student_id);
        value = result.GetString(0);
        m_txtTaskString->SetValue(value);
        m_lblTaskNumber->SetLabel(wxString::Format("%d", (int)value.Length()));
    }
    catch(wxSQLite3Exception e)
    {
        wxLogError(e.GetMessage());
    }
}

bool NotesFrame::CheckString(wxString value)
{

    wxRegEx rep("\t");
    rep.Replace(&value, "");
    m_txtTaskString->ChangeValue(value);
    m_txtTaskString->SetInsertionPoint(value.Length());
    m_lblTaskNumber->SetLabel(wxString::Format("%d", (int)value.Length()));
    Layout();
    
    wxString regex = wxString::Format("[^%c%c%c%c%c]+", _r, _u, _f, _m, _z);
    wxRegEx reg(regex, wxRE_ICASE);
    
    if(reg.Matches(value))
    {
        wxLogMessage(value);
        return false;
    }
    return true;
}

bool NotesFrame::CalculateStatistic(int idx, wxString task_string)
{
    int right=0,wrong=0,satisfied=0,deficient=0,additional=0;
    
    wxString::iterator it = task_string.begin();
    while(it != task_string.end())
    {
        char c = *it;
        if(c == _f)wrong++;
        else if(c == _m)satisfied++;
        else if(c == _u)deficient++;
        else if(c == _r)right++;
        else if(c == _z)additional++;
        it++;
    }
    
    double made = (((double)right + (double)satisfied + (double)wrong)/(double)m_tCount)*100.00;
    double correct = ((double)right + (double)satisfied)/(double)m_tCount*100.00;
    double unmade = (((double)wrong + (double)satisfied)/(double)m_tCount)*100.00;
    
    m_listStudetns->SetItem(idx, 4, wxString::Format("%d",(right+satisfied+wrong)));
    m_listStudetns->SetItem(idx, 5, wxString::Format("%.2f%%", made));
    
    m_listStudetns->SetItem(idx, 6, wxString::Format("%d", right));
    m_listStudetns->SetItem(idx, 7, wxString::Format("%d", satisfied));
    m_listStudetns->SetItem(idx, 8, wxString::Format("%.2f%%", correct));
    
    m_listStudetns->SetItem(idx, 9, wxString::Format("%d", wrong));
    m_listStudetns->SetItem(idx, 10, wxString::Format("%d", deficient));
    m_listStudetns->SetItem(idx, 11, wxString::Format("%.2f%%", unmade));
    
    m_listStudetns->SetItem(idx, 12, wxString::Format("%s", (made >= 60 && correct >= 40)?"TRUE":"FALSE"));
    
    return made >= 60 && correct >= 40;
}
