/**
 @file config.cpp
 @brief Implementierung der Klasse Config.
 @author Alexey Elimport
 @version 0.1
 @date 16.05.2012
 */

#include "config.h"
#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/filepicker.h>
#include <wx/listctrl.h>
#include <wx/clrpicker.h>
#include <wx/html/htmltag.h>


Config::Config()
{  
    m_config = new wxFileConfig(_T("Config"), _T("STG"), _T("Config.cfg"), wxEmptyString, wxCONFIG_USE_RELATIVE_PATH);
}

Config::~Config()
{
	delete m_config;
}
//----------------------------------------------------------------------
// Lesen der Einstellungen
//----------------------------------------------------------------------


void Config::ReadSettings(wxWindow* w, wxString path)
{
	m_mutex.Lock();
	m_config->SetPath(wxString("/") + path);
	wxString oldPath = m_config->GetPath();
	wxWindowList children = w->GetChildren();
	wxWindowList::iterator i = children.begin();

	for(i=children.begin();i!=children.end();i++)
	{
		wxWindow* item = *i;
		// wxTextCtrl
		if(item->IsKindOf(CLASSINFO(wxTextCtrl)))
		{
			wxTextCtrl* _item = wxDynamicCast(item, wxTextCtrl);
			_item->SetValue(m_config->Read(_item->GetName()));
		}
		// wxListBox
		else if(item->IsKindOf(CLASSINFO(wxListBox)))
		{
			wxString oldPath = m_config->GetPath();
			wxListBox* _item = wxDynamicCast(item, wxListBox);
			long value = 0;
			long flag = 0;
			value = m_config->ReadLong(_item->GetName(), 0);
			wxString name = _item->GetName();
			if(name.Cmp("LanguageList") == 0 || name.Cmp("OPCServer") == 0)
			{
				continue; //Sonderbehandlung in den Panels selbst!
			}
			if(_item->HasMultipleSelection())
			{
				for(int k=0;k<(int)_item->GetCount();k++)
				{
					flag = (long)pow((float)2, (float)k);
					if((flag & value) == flag)
						_item->SetSelection(k);
				}
			}
			else
			{
				value = m_config->ReadLong(_item->GetName(), 0);
				if(value >= 0 && value < (int)_item->GetCount())
					_item->SetSelection(value);
			}
			wxArrayString choices = _item->GetStrings();
			if(choices.size() == 0)
			{
				wxString s;
				long dummy;

				m_config->SetPath(_item->GetName());
				bool next = m_config->GetFirstEntry(s, dummy);
				while(next)
				{
					_item->Append(m_config->Read(s));
					next = m_config->GetNextEntry(s, dummy);
				}
			}
			m_config->SetPath(oldPath);
			wxCommandEvent evt(wxEVT_COMMAND_LISTBOX_SELECTED, _item->GetId());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxCheckBox
		else if(item->IsKindOf(CLASSINFO(wxCheckBox)))
		{
			wxCheckBox* _item = wxDynamicCast(item, wxCheckBox);
			_item->SetValue(m_config->ReadBool(_item->GetName(), false));
			wxCommandEvent evt(wxEVT_COMMAND_CHECKBOX_CLICKED, _item->GetId());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxComboBox
		else if(item->IsKindOf(CLASSINFO(wxComboBox)))
		{
			wxComboBox* _item = wxDynamicCast(item, wxComboBox);
			_item->SetSelection(m_config->ReadLong(_item->GetName(), 0));
		}
		// wxChoice
		else if(item->IsKindOf(CLASSINFO(wxChoice)))
		{
			wxChoice* _item = wxDynamicCast(item, wxChoice);
			_item->SetSelection(m_config->ReadLong(_item->GetName(), 0));
			wxCommandEvent evt(wxEVT_COMMAND_CHOICE_SELECTED, _item->GetId());
			evt.SetInt(_item->GetSelection());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxRadioBox
		else if(item->IsKindOf(CLASSINFO(wxRadioBox)))
		{
			wxRadioBox* _item = wxDynamicCast(item, wxRadioBox);
			_item->Select(m_config->ReadLong(_item->GetName(), 1));
			wxCommandEvent evt(wxEVT_COMMAND_RADIOBUTTON_SELECTED, _item->GetId());
			evt.SetInt(_item->GetSelection());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxRadioButton
		else if(item->IsKindOf(CLASSINFO(wxRadioButton)))
		{
			wxRadioButton* _item = wxDynamicCast(item, wxRadioButton);
			_item->SetValue(m_config->ReadBool(_item->GetName(),1));
			wxCommandEvent evt(wxEVT_COMMAND_RADIOBOX_SELECTED, _item->GetId());
			evt.SetInt(_item->GetValue());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxSpinCtrl
		else if(item->IsKindOf(CLASSINFO(wxSpinCtrl)))
		{
			wxSpinCtrl* _item = wxDynamicCast(item, wxSpinCtrl);
			_item->SetValue(m_config->ReadLong(_item->GetName(),0));
			wxCommandEvent evt(wxEVT_COMMAND_SPINCTRL_UPDATED, _item->GetId());
			evt.SetInt(_item->GetValue());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxSpinCtrlDouble
		else if(item->IsKindOf(CLASSINFO(wxSpinCtrlDouble)))
		{
			wxSpinCtrlDouble* _item = wxDynamicCast(item, wxSpinCtrlDouble);
			_item->SetValue(m_config->ReadDouble(_item->GetName(),0));
			wxSpinDoubleEvent evt(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, _item->GetId());
			evt.SetValue(_item->GetValue());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxDirPickerCtrl
		else if(item->IsKindOf(CLASSINFO(wxDirPickerCtrl)))
		{
		    wxDirPickerCtrl* _item = wxDynamicCast(item, wxDirPickerCtrl);
		    _item->SetPath(m_config->Read(_item->GetName()));
			wxFileDirPickerEvent evt(wxEVT_COMMAND_DIRPICKER_CHANGED, _item, _item->GetId(), _item->GetPath());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxFilePickerCtrl
		else if(item->IsKindOf(CLASSINFO(wxFilePickerCtrl)))
		{
			wxFilePickerCtrl *_item = wxDynamicCast(item, wxFilePickerCtrl);
			_item->SetPath(m_config->Read(_item->GetName()));
			wxFileDirPickerEvent evt(wxEVT_COMMAND_FILEPICKER_CHANGED, _item, _item->GetId(), _item->GetPath());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxSlider
		else if(item->IsKindOf(CLASSINFO(wxSlider)))
		{
			wxSlider* _item = wxDynamicCast(item, wxSlider);
			_item->SetValue(m_config->ReadLong(_item->GetName(), 50));
			wxCommandEvent evt(wxEVT_COMMAND_SLIDER_UPDATED, _item->GetId());
			evt.SetInt(_item->GetValue());
			_item->GetParent()->GetEventHandler()->AddPendingEvent(evt);
		}
		// wxColourPicker
		else if(item->IsKindOf(CLASSINFO(wxColourPickerCtrl)))
		{
			wxColourPickerCtrl* _item = wxDynamicCast(item, wxColourPickerCtrl);
			_item->SetColour(m_config->Read(_item->GetName(), ""));
		}
	}
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
}

//----------------------------------------------------------------------
// Speichern der Einstellungen
//----------------------------------------------------------------------

void Config::WriteSettings(wxWindow* w, wxString path)
{
	m_mutex.Lock();
	m_config->SetPath(wxString("/") + path);
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(wxString(_T("/")) + w->GetName());
	wxWindowList children = w->GetChildren();
	wxWindowList::iterator j = children.begin();
	for(;j!=children.end();j++)
	{
		wxWindow* item = *j;
		// wxTextCtrl
		if(item->IsKindOf(CLASSINFO(wxTextCtrl)))
		{
			wxTextCtrl* _item = wxDynamicCast(item, wxTextCtrl);
			if(_item->GetName().IsEmpty() || _item->GetName().Cmp("text") == 0)
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetValue());
		}
		// wxListBox
		else if(item->IsKindOf(CLASSINFO(wxListBox)))
		{
			wxListBox* _item = wxDynamicCast(item, wxListBox);
#ifdef __WXMAC__
			_item->SetWindowStyle(wxBORDER_SIMPLE);
#endif
			wxString name = _item->GetName();
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
            //Spracheinstellungen
            if(_item->GetName().Cmp("LanguageList") == 0)
            {
                wxArrayInt selections;
                _item->GetSelections(selections);
                if(selections.Count() <= 0)
                    return;
                int lang;
                switch(selections[0])
                {
                    case 0: lang = wxLANGUAGE_ENGLISH; break;
                    case 1: lang = wxLANGUAGE_GERMAN; break;
                    case 2: lang = wxLANGUAGE_RUSSIAN; break;
                    default: lang = wxLANGUAGE_ENGLISH; break;
                }
                m_config->SetPath("/Language");
                m_config->Write("id", lang);
                m_config->SetPath(oldPath);
            }
            else if(_item->GetName().Cmp("OPCServer") == 0)
			{
                int selection = _item->GetSelection();
                if(selection < 0)
                    return;
                
                m_config->SetPath("/OPC");
                m_config->Write("OPCServer", _item->GetString(selection));
                m_config->SetPath(oldPath);
            }
			else if(_item->GetName().Cmp("ActiveFeatures") == 0)
			{
				 wxArrayInt selections;
                _item->GetSelections(selections);
                if(selections.Count() == 0)
                    return;
				int flag = 0;
				for(size_t i = 0; i < selections.Count(); i++)
				{
					flag += pow(2.0, selections[i]);
				}
				m_config->SetPath("/DynamicObject");
                m_config->Write("ActiveFeatures", flag);
                m_config->SetPath(oldPath);
			}
			
			else
            {
                //Maskeneinstellungen
                wxArrayInt selections;
                wxString oldPath = m_config->GetPath();
                long flag = 0;
				if(_item->HasMultipleSelection())
				{
					_item->GetSelections(selections);
					for(int k = 0;k<(int)_item->GetCount();k++)
					{
						if(_item->IsSelected(k))
							flag += (long)pow(2.0,k);
					}
					m_config->Write(_item->GetName(), flag);
				}
				else
				{
					m_config->Write(_item->GetName(), _item->GetSelection());
				}
                wxArrayString choices = _item->GetStrings();


                if (choices.size() != 0) {
                    m_config->SetPath(_item->GetName());
                    for(int i=0;i<(int)choices.size();i++)
                        m_config->Write(wxString::Format("%d", i), choices[i]);
                }
                m_config->SetPath(oldPath);
            }
		}
        // wxListBox
        else if(item->IsKindOf(CLASSINFO(wxListView)))
		{
			wxListView* _item = wxDynamicCast(item, wxListView);
#ifdef __WXMAC__
			_item->SetWindowStyle(wxBORDER_SIMPLE);
#endif
			wxString name = _item->GetName();
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			wxString oldPath = m_config->GetPath();
            wxString newPath = _item->GetName();
			m_config->SetPath(newPath);
			for(int k = 0;k<_item->GetItemCount();k++)
			{
                wxString itemText = _item->GetItemText(k,0);
                wxColour itemColor = _item->GetItemTextColour(k);

                m_config->Write(wxString::Format(wxT("Color%i"),k), itemColor.GetAsString());
                m_config->Write(wxString::Format(wxT("Name%i"),k), itemText);
			}

			m_config->SetPath(oldPath);
		}
		// wxChoice
		else if(item->IsKindOf(CLASSINFO(wxChoice)))
		{
			wxChoice* _item = wxDynamicCast(item, wxChoice);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetSelection());
		}
		// wxCheckBox
		else if(item->IsKindOf(CLASSINFO(wxCheckBox)))
		{
			wxCheckBox* _item = wxDynamicCast(item, wxCheckBox);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetValue());
		}
		// wxComboBox
		else if(item->IsKindOf(CLASSINFO(wxComboBox)))
		{
			wxComboBox* _item = wxDynamicCast(item, wxComboBox);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetSelection());
		}
		// wxRadioBox
		else if(item->IsKindOf(CLASSINFO(wxRadioBox)))
		{
			wxRadioBox* _item = wxDynamicCast(item, wxRadioBox);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetSelection());
		}
		// wxRadioButton
		else if(item->IsKindOf(CLASSINFO(wxRadioButton)))
		{
			wxRadioButton* _item = wxDynamicCast(item, wxRadioButton);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetValue());
		}
		// wxSpinCtrl
		else if(item->IsKindOf(CLASSINFO(wxSpinCtrl)))
		{
			wxSpinCtrl* _item = wxDynamicCast(item, wxSpinCtrl);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetValue());
		}
		// wxSpinCtrlDouble
		else if(item->IsKindOf(CLASSINFO(wxSpinCtrlDouble)))
		{
			wxSpinCtrlDouble* _item = wxDynamicCast(item, wxSpinCtrlDouble);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetValue());
		}
		// wxDirPickerCtrl
		else if(item->IsKindOf(CLASSINFO(wxDirPickerCtrl)))
		{
		    wxDirPickerCtrl* _item = wxDynamicCast(item, wxDirPickerCtrl);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			if(_item->HasFlag(wxDIRP_USE_TEXTCTRL))
				m_config->Write(_item->GetName(), _item->GetTextCtrlValue());
			else
            {
                if(_item->GetPath().IsEmpty())
                    m_config->Write(_item->GetName(), wxGetCwd());
                else
                    m_config->Write(_item->GetName(), _item->GetPath());
            }
		}
		// wxFilePickerCtrl
		else if(item->IsKindOf(CLASSINFO(wxFilePickerCtrl)))
		{
			wxFilePickerCtrl* _item = wxDynamicCast(item, wxFilePickerCtrl);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			if(_item->HasFlag(wxFLP_USE_TEXTCTRL))
				m_config->Write(_item->GetName(), _item->GetTextCtrlValue());
			else
				m_config->Write(_item->GetName(), _item->GetPath());
		}
		// wxSlider
		else if(item->IsKindOf(CLASSINFO(wxSlider)))
		{
			wxSlider* _item = wxDynamicCast(item, wxSlider);
			if(_item->GetName().IsEmpty())
			{
				continue;
			}
			m_config->Write(_item->GetName(), _item->GetValue());
		}
		// wxColourPicker
		else if(item->IsKindOf(CLASSINFO(wxColourPickerCtrl)))
		{
			if(w->GetName().Cmp("Logger") == 0 || w->GetName().Cmp("DynamicObject") == 0)
			{
				wxColourPickerCtrl* _item = wxDynamicCast(item, wxColourPickerCtrl);
				if(_item->GetName().IsEmpty())
				{
					continue;
				}
				m_config->Write(_item->GetName(), _item->GetColour());
			}
		}
	}
	m_config->SetPath(oldPath);
	m_config->Flush();
	m_mutex.Unlock();
}

void Config::WriteSettings()
{
    m_mutex.Lock();
    m_config->Flush();
	m_mutex.Unlock();
}

//----------------------------------------------------------------------
// Lesen der Werte
//----------------------------------------------------------------------

bool Config::ReadBool(wxString path, wxString key, bool def)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	bool v = m_config->ReadBool(key, def);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return v;
}

double Config::ReadDouble(wxString path, wxString key, double def)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	double v = m_config->ReadDouble(key, def);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return v;
}

long Config::ReadLong(wxString path, wxString key, long def)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	long v = m_config->ReadLong(key, def);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return v;
}

wxString Config::Read(wxString path, wxString key, wxString def)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	wxString v = m_config->Read(key, def);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return v;
}

void Config::WriteBool(wxString path, wxString key, bool value)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	m_config->Write(key, value);
	m_config->SetPath(oldPath);
	m_config->Flush();
	m_mutex.Unlock();
}

void Config::WriteDouble(wxString path, wxString key, double value)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	m_config->Write(key, value);
	m_config->SetPath(oldPath);
	m_config->Flush();
	m_mutex.Unlock();
}

void Config::WriteLong(wxString path, wxString key, long value)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	m_config->Write(key, value);
	m_config->SetPath(oldPath);
	m_config->Flush();
	m_mutex.Unlock();
}

void Config::Write(wxString path, wxString key, wxString value)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	m_config->Write(key, value);
	m_config->SetPath(oldPath);
	m_config->Flush();
	m_mutex.Unlock();
}

void Config::DeleteEntry(wxString path, wxString key, bool deleteGroupIfEmpty)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	m_config->DeleteEntry(key, deleteGroupIfEmpty);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
}

void Config::DeleteGroup(wxString name)
{
	m_mutex.Lock();
	m_config->DeleteGroup(name);
	m_mutex.Unlock();
}

bool Config::GetFirstEntry(wxString path, wxString &value, long &index)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	bool next;
	m_config->SetPath(path);
	next = m_config->GetFirstEntry(value, index);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return next;
}

bool Config::GetNextEntry(wxString path, wxString &value, long &index)
{
	m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	bool next;
	m_config->SetPath(path);
	next = m_config->GetNextEntry(value, index);
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return next;
}

unsigned int Config::GetNumberOfEntries(wxString path)
{
    m_mutex.Lock();
	wxString oldPath = m_config->GetPath();
	m_config->SetPath(path);
	unsigned int entries = m_config->GetNumberOfEntries(false); //nicht rekursiv
	m_config->SetPath(oldPath);
	m_mutex.Unlock();
	return entries;
}

wxStringToStringHashMap Config::GetGroupEntries(wxString path)
{
	m_mutex.Lock();
	wxStringToStringHashMap values;

	m_config->SetPath(wxString::Format("/%s",path));
	wxString key, value;
	long dummy;
	bool next = m_config->GetFirstEntry(key, dummy);
	value = m_config->Read(key);
	values[key] = value;
	
	while(next)
	{
		next = m_config->GetNextEntry(key, dummy);
		value = m_config->Read(key);
		values[key] = value;
	}
	m_mutex.Unlock();
	return values;
}