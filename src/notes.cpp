#include "notes.h"

#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/stdpaths.h>

IMPLEMENT_APP(Notes);

bool Notes::OnInit()
{
#if defined __linux__ || defined _MSC_VER
	wxSetWorkingDirectory(GetCwd());
#endif
    m_frame = new NotesFrame(NULL);
    SetTopWindow(m_frame);
	
    m_frame->Show();
    return true;
}

wxString Notes::GetCwd()
{
	wxString cwd = wxStandardPaths::Get().GetExecutablePath();
	wxString ncwd =
	cwd.erase(cwd.find_last_of(wxFileName::GetPathSeparator())+1,
									   cwd.length() - ( cwd.find_last_of(wxFileName::GetPathSeparator())+1)) ;
	return ncwd;
}
